import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatPage } from '../chat/chat';

/**
 * Generated class for the WaitingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-waiting',
  templateUrl: 'waiting.html',
})
export class WaitingPage {
  name
  add
  stat
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.name = this.navParams.get('name');
    this.add = this.navParams.get('address');
    this.stat = this.navParams.get('status');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WaitingPage');
  }
  gotochat(name){
    let data = {
      names: name
    }
    this.navCtrl.push(ChatPage, data);
  }
}
