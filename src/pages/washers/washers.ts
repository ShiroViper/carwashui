import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LocationPage } from '../location/location';

/**
 * Generated class for the WashersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-washers',
  templateUrl: 'washers.html',
})
export class WashersPage {
  responseTxt
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertController:AlertController) {
    this.showTable();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WashersPage');
  }
  showTable(){
    this.responseTxt = [
        {
          "name": "John Doe",
          "address": "Talamban Cebu City",
          "status": "Available"
        },
        {
          "name": "Mario Alejado",
          "address": "Banilad Cebu City",
          "status": "Booked"
        },
        {
          "name": "Petey Cruiser",
          "address": "Liloan Cebu City",
          "status": "Booked"
        },
        {
          "name": "Anna Sthesia",
          "address": "Minglanilla Cebu City",
          "status": "Available"
        },
        {
          "name": "Rex Sala",
          "address": "Minglanilla Cebu City",
          "status": "Available"
        }
    ]
  }
  goToOtherPage(name,address,status){
    console.log(name, status), address;
    if(status == "Booked"){
      let alert = this.alertController.create({
        title: "Opss!!",
        message:"This washer is currently unavailable please book at another time.",
        buttons:[
          {
            text:"Okay"
          }],
          cssClass:'alertCustomCss'
      });
      alert.present();
    }else{
      let data ={
        names: name,
        stat: status,
        add: address
      } 
      this.navCtrl.push(LocationPage, data);    
    }
  
  }
}
