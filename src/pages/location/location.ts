import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WaitingPage } from '../waiting/waiting';

/**
 * Generated class for the LocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  name
  status
  address
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.name = this.navParams.get('names');
    this.address = this.navParams.get('add');
    this.status = this.navParams.get('stat');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
  }
  gotowaitingPage(){
    this.name = this.navParams.get('names');
    this.address = this.navParams.get('add');
    this.status = this.navParams.get('stat');
    let data = {
      name: this.name,
      address: this.status,
      status: this.status
    }
    this.navCtrl.push(WaitingPage, data);
  }

}
