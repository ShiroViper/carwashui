import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  names
  public msg = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.names = this.navParams.get('names');
    console.log(this.names);

  }


  sendMessage(message){
    this.msg.push(message);
    
  }

}
