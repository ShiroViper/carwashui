import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, private alertController:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  GoLog(username, password){
    console.log(username);
    console.log(password);
    if(username == "user" && password == "password"){
      let data = {
        user: username 
      }
      this.navCtrl.setRoot(HomePage, data);
    }else{
      console.log('wrong password');
      let alert = this.alertController.create({
        title: "Error",
        message:"You've enter a wrong password.",
        buttons:[
          {
            text:"Okay"
          }]
      });
      alert.present();
    }
  }
  goToRegister(){
    this.navCtrl.push(RegisterPage)
  }
  

}
