import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WashersPage } from '../washers/washers';

/**
 * Generated class for the BookPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book',
  templateUrl: 'book.html',
})
export class BookPage {
  responseTxt
  name
  wash
  shine
  mat
  leather
  interior
  data1
  data2
  data3
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.name = this.navParams.get('name');
    this.wash = this.navParams.get('wash');
    this.shine = this.navParams.get('shine');
    this.mat = this.navParams.get('mat');
    this.leather = this.navParams.get('leather');
    this.interior = this.navParams.get('interior');
  }
  
  booknow(option,one,two){
    if(option){
      this.data1 = this.navParams.get('mat');
    }
    if(one){
      this.data2  = this.navParams.get('leather');
    }
    if(two){
      this.data3 = this.navParams.get('interior');
    }
    let data = {
      name: this.navParams.get('name'),
      mat : this.data1,
      leather: this.data2,
      interior: this.data3
    }
    this.navCtrl.push(WashersPage,data);
  }


}
