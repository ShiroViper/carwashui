import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { BookPage } from '../book/book';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  service: Array <any> = [];
  i
  constructor(public navCtrl: NavController, private alertController:AlertController) {
    this.show();
  }
  show(){
    this.service = [
      {
        "name": "Small",
        "wash": "150.00 Php",
        "shine": "300.00 Php"
      },
      {
        "name": "Medium",
        "wash": "170.00 Php",
        "shine": "400.00 Php"
      },
      {
        "name": "Large",
        "wash": "190.00 Php",
        "shine": "500.00 Php"
      }

    ]
  }
  Service(status){
    if(status == "Small"){
      let alert = this.alertController.create({
        title: "Small",
        message:"<center>Wash: 150 Php <br> exterior, body wash, air blow dry, tire black <br><br> Shine: 300 Php<br> exterior body wash plus wax n 'buff air blow dry and tire black </center>",
        buttons:[
          {
            text:"cancel"
          },
          {
            text: "okay",
            handler: () => {
              let data = {
                  name: "Small",
                  wash: "150 Php",
                  shine: "300 Php",
                  mat: "65 Php",
                  leather: "400 Php",
                  interior: "75 Php"
                }
                this.navCtrl.push(BookPage,data);
              
            }
          }
        ],
        cssClass:'alertCustomCss'
      });
      alert.present();
    }
    if(status == "Medium"){
      let alert = this.alertController.create({
        title: "Medium",
        message:"<center>Wash: 170 Php <br> exterior, body wash, air blow dry, tire black <br><br> Shine: 400 Php<br> exterior body wash plus wax n 'buff air blow dry and tire black </center>",
        buttons:[
          {
            text:"cancel"
          },
          {
            text: "okay",
            handler: () => {
              let data = {
                  name: "Medium",
                  wash: "170 Php",
                  shine: "400 Php",
                  mat: "95 Php",
                  leather: "500 Php",
                  interior: "75 Php"
                }
                this.navCtrl.push(BookPage,data);
              
            }
          }
        ],
        cssClass:'alertCustomCss'
      });
      alert.present();
    }
    if(status == "Large"){
      let alert = this.alertController.create({
        title: "Large",
        message:"<center>Wash: 190 Php<br> exterior, body wash, air blow dry, tire black <br><br> Shine: 500 Php<br> exterior body wash plus wax n 'buff air blow dry and tire black </center>",
        buttons:[
          {
            text:"cancel"
          },
          {
            text: "okay",
            handler: () => {
              let data = {
                  name: "Large",
                  wash: "190 Php",
                  shine: "500 Php",
                  mat: "115 Php",
                  leather: "550 Php",
                  interior: "85 Php"
                }
                this.navCtrl.push(BookPage,data);
              
            }
          }
        ],
        cssClass:'alertCustomCss'
      });
      alert.present();
    }
  }

}
